/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sessions;

import com.entity.Request;
import com.exception.SendAndRecieveException;
import com.jpa.RequestJpaController;
import com.jpa.exceptions.RollbackFailureException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import javax.transaction.UserTransaction;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author TMshaisa
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class SendAndRecieve implements SendAndRecieveLocal {

    @Resource
    private UserTransaction utx;

    @PersistenceContext(name = "ABLPU")
    private EntityManager em;

    private static final String SERVER_HOST = "196.37.22.179";
    private static final int PORT = 9011;

    @Override
    public String sendStringXml(String xml) throws SendAndRecieveException {
        //Convert 
        //Get User 
        //Write to a file
        int i =12345;

        Request request = new RequestJpaController(utx, em).findRequest(i);
        String sendReturn = null;
        if (request != null) {
            try {
                //Convert to xml and send to server
                Marshaller mash = JAXBContext.newInstance(Request.class).createMarshaller();
                StringWriter sw = new StringWriter();
                mash.marshal(request, sw);
                final Socket socket = new Socket(SERVER_HOST, PORT);
                final PrintWriter pw = new PrintWriter(socket.getOutputStream(), true);
                pw.write(sw.toString());
                System.out.println("Send to server::"+sw.toString());
                
                //Display server response
                sendReturn=displayStringXml(socket);
                

            } catch (JAXBException ex) {
                Logger.getLogger(SendAndRecieve.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(SendAndRecieve.class.getName()).log(Level.SEVERE, null, ex);
            }
            

        }
        return sendReturn;
        // Add business logic below. (Right-click in editor and choose
        // "Insert Code > Add Business Method")
    }
    
    
    private static final String FILENAME = "C:\\ReadMe.txt";
    @Override
    public String displayStringXml(Socket socket) throws SendAndRecieveException {
        String disp = null;
        try {
            
            final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            socket.setSoTimeout(10000); 
            
            BufferedWriter bw = null;
	    FileWriter fw = null;
            
            fw = new FileWriter(FILENAME);
	    bw = new BufferedWriter(fw);

            String response;
            while ((response = reader.readLine()) != null) {
                //Write to a file
                bw.write(response);
                bw.newLine();
                disp+=response;
                System.out.println(":::::  "+response);
            }
            bw.close();
            
            //Convert to object and Persist to DB
            Request request;
            StringReader sr = new StringReader(socket.toString());
            System.out.println("Server returned :::: "+sr);
            Unmarshaller unMarsh = JAXBContext.newInstance(Request.class).createUnmarshaller();
            request =(Request) unMarsh.unmarshal(sr);
            new RequestJpaController(utx, em).create(request);
            
        } catch (JAXBException ex) {
            Logger.getLogger(SendAndRecieve.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RollbackFailureException ex) {
            Logger.getLogger(SendAndRecieve.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(SendAndRecieve.class.getName()).log(Level.SEVERE, null, ex);
        }
        
       return disp; 
    }
}
