/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sessions;

import com.entity.Request;
import com.exception.SendAndRecieveException;
import java.net.Socket;
import javax.ejb.Local;

/**
 *
 * @author TMshaisa
 */
@Local
public interface SendAndRecieveLocal {
 
    /**
     *
     * @param xml
     * @throws SendAndRecieveException
     */
    public String sendStringXml(String xml) throws SendAndRecieveException;
    public String displayStringXml(Socket socket) throws SendAndRecieveException;

}
