/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exception;

/**
 *
 * @author TMshaisa
 */
public class SendAndRecieveException extends Exception {

    /**
     * Creates a new instance of <code>SendAndRecieveException</code> without
     * detail message.
     */
    public SendAndRecieveException() {
    }

    /**
     * Constructs an instance of <code>SendAndRecieveException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public SendAndRecieveException(String msg) {
        super(msg);
    }
}
