/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author TMshaisa
 */
@Entity
@Table(name = "REQUEST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Request.findAll", query = "SELECT r FROM Request r"),
    @NamedQuery(name = "Request.findByUserpin", query = "SELECT r FROM Request r WHERE r.userpin = :userpin"),
    @NamedQuery(name = "Request.findByDeviceid", query = "SELECT r FROM Request r WHERE r.deviceid = :deviceid"),
    @NamedQuery(name = "Request.findByDeviceser", query = "SELECT r FROM Request r WHERE r.deviceser = :deviceser"),
    @NamedQuery(name = "Request.findByDevicever", query = "SELECT r FROM Request r WHERE r.devicever = :devicever"),
    @NamedQuery(name = "Request.findByTranstype", query = "SELECT r FROM Request r WHERE r.transtype = :transtype")})
public class Request implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "USERPIN")
    private Integer userpin;
    @Column(name = "DEVICEID")
    private Integer deviceid;
    @Size(max = 255)
    @Column(name = "DEVICESER")
    private String deviceser;
    @Size(max = 255)
    @Column(name = "DEVICEVER")
    private String devicever;
    @Size(max = 255)
    @Column(name = "TRANSTYPE")
    private String transtype;

    public Request() {
    }

    public Request(Integer userpin) {
        this.userpin = userpin;
    }

    public Integer getUserpin() {
        return userpin;
    }

    public void setUserpin(Integer userpin) {
        this.userpin = userpin;
    }

    public Integer getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(Integer deviceid) {
        this.deviceid = deviceid;
    }

    public String getDeviceser() {
        return deviceser;
    }

    public void setDeviceser(String deviceser) {
        this.deviceser = deviceser;
    }

    public String getDevicever() {
        return devicever;
    }

    public void setDevicever(String devicever) {
        this.devicever = devicever;
    }

    public String getTranstype() {
        return transtype;
    }

    public void setTranstype(String transtype) {
        this.transtype = transtype;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userpin != null ? userpin.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Request)) {
            return false;
        }
        Request other = (Request) object;
        if ((this.userpin == null && other.userpin != null) || (this.userpin != null && !this.userpin.equals(other.userpin))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entity.Request[ userpin=" + userpin + " ]";
    }
    
}
